package nl.utwente.di.bookQuote;

public class Quoter {

    public double getBookPrice(String s) {

        double x;
        String y = s;

        switch (y) {
            case "1":
                x = 10.0;
                break;
            case "2":
                x = 45.0;
                break;
            case "3":
                x = 20.0;
                break;
            case "4":
                x = 35.0;
                break;
            case "5":
                x = 50.0;
                break;
            default:
                x = 0.0;
        }

        return x;

        }
}

